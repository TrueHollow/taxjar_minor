const chai = require('chai');
chai.use(require('chai-http'));

const { expect } = chai;
const { app, listener } = require('../src/index');

describe('App test', () => {
  before(async () => {});
  after(async () => {
    listener.close();
  });
  it('test POST method for /upload', async () => {
    const data = {
      street: '1106 Grandview Rd',
      city: 'Ardmore',
      state: 'OK',
      zip: '73401',
    };
    const res = await chai
      .request(app)
      .post('/taxjar')
      .send(data);
    expect(res).to.have.status(200);
  });
});
