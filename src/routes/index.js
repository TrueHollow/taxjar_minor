const rp = require('request-promise-native');
const logger = require('../common/Logger')('src/routes/index.js');

const { AuthorizationBearer } = process.env;

module.exports = app => {
  app.get('/', (req, res) => {
    res.json({
      message: 'taxjar',
    });
  });

  app.post('/taxjar', async (req, res) => {
    try {
      const { street, city, state, zip } = req.body;
      const url = `https://api.taxjar.com/v2/rates/?street=${street}&city=${city}&state=${state}&zip=${zip}`;
      logger.debug(`Url: ${url}`);
      const data = await rp({
        method: 'GET',
        url,
        headers: {
          Authorization: `Bearer ${AuthorizationBearer}`,
        },
        json: true,
      });
      res.json(data);
    } catch (e) {
      res.statusCode(500);
      res.json({ error: 'Error' });
      logger.error(e);
    }
  });
};
